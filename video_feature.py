from engine.FaceProcess import FaceProcess
from engine.ColorDetection import ColorDetection
from engine.GoogleVideoProcessing import GoogleVideoProcessing
from engine import audio_feature
from engine import func
from engine.constant import *
from upgrade_feature import convert_advanced_csv
import json
import ast
import sys
import os


class_process = FaceProcess(face_engine='dnn')
class_color = ColorDetection()
class_google_video = GoogleVideoProcessing()


KEY_REF_CSV = ['mediaID', 'firstSeen', 'lastSeen', 'duration', 'properties.width', 'properties.height',
               'impressionShare', 'category', 'mediaType']


def analysis_video(video_filename):
    print('\tMost Color Detection ...')
    ret_json_color, video_info = class_color.detect_color_video(video_filename)
    func.write_text(OUTPUT_JSON_COLOR, json.dumps(ret_json_color, indent=4))

    print('\tFace and Age/Gender Detection ...')
    ret_json_face = class_process.process_video(video_filename, scale_factor=0.5, show_video=False, write_video=False)
    func.write_text(OUTPUT_JSON_FACE, json.dumps(ret_json_face, indent=4))

    print('\tGoogle API Extraction ...')
    class_google_video.process_video(video_filename, OUTPUT_JSON_GOOGLE)
    ret_json_google = func.read_json(OUTPUT_JSON_GOOGLE)

    print('\tAudio Mood Detection ...')
    audio_file = 'temp_audio.mp3'
    ret_audio = audio_feature.convert_video_to_audio(in_video_file=video_filename, out_audio_file=audio_file)
    if ret_audio:
        audio_mood, audio_channels, audio_sample_width, audio_sample_rate = audio_feature.get_audio_feature(audio_file)
    else:
        audio_mood, audio_channels, audio_sample_width, audio_sample_rate = "", 0, 0, 0

    func.rm_file(audio_file)

    (vid_width, vid_height, vid_duration) = video_info
    json_total = {
        "video_info": {
            "width": vid_width,
            "height": vid_height,
            "duration": vid_duration
        },
        "audio_info": {
            "mood": audio_mood,
            "channels": audio_channels,
            "sample_width": audio_sample_width,
            "sample_rate": audio_sample_rate
        }
    }
    json_total.update(ret_json_color)
    json_total.update(ret_json_face)
    json_total.update(ret_json_google)
    func.write_text(OUTPUT_JSON_ALL, json.dumps(json_total, indent=4))

    return json_total


def extract_features(json_video):

    def avg_gender_age(start_frame, end_frame):
        cnt_gender_male, cnt_gender_female = 0, 0
        sum_age, cnt_face = 0, 0
        for frame_ind in range(start_frame, end_frame):
            frame_faces = dict_face[f'frame{frame_ind}']
            if len(frame_faces) > 0:
                for face_ind in range(len(frame_faces)):
                    age_range = ast.literal_eval(frame_faces[face_ind]['age'])
                    sum_age += (age_range[0] + age_range[1]) / 2
                    cnt_face += 1
                    if frame_faces[face_ind]['gender'] == 'Male':
                        cnt_gender_male += 1
                    else:
                        cnt_gender_female += 1

        if cnt_gender_male + cnt_gender_female > 0:
            if cnt_gender_male > cnt_gender_female:
                avg_gender = GENDER_MALE
            else:
                avg_gender = GENDER_FEMALE

            if sum_age / cnt_face < 6:
                avg_age = AGE_BABY
            elif sum_age / cnt_face < 15:
                avg_age = AGE_TEEN
            elif sum_age / cnt_face < 45:
                avg_age = AGE_ADULT
            else:
                avg_age = AGE_OLD
        else:
            avg_gender = GENDER_NA
            avg_age = ''

        return avg_age, avg_gender

    # print(json_video.keys())
    feature_list = {}
    dict_color = json_video['most_color_detection']
    dict_face = json_video['face_detection']
    dict_google = json_video['annotation_results']
    if 'face_detection_annotations' in dict_google[0]:
        dict_general_google = dict_google[0]
    elif len(dict_google) > 1 and 'face_detection_annotations' in dict_google[1]:
        dict_general_google = dict_google[1]
    else:
        dict_general_google = {}

    if 'speech_transcriptions' in dict_google[0]:
        dict_speech_google = dict_google[0]
    elif len(dict_google) > 1 and 'speech_transcriptions' in dict_google[1]:
        dict_speech_google = dict_google[1]
    else:
        dict_speech_google = {}

    # ------------------------------ Color features ----------------------------------
    avg_brightness, avg_r, avg_g, avg_b = 0, 0, 0, 0
    for i in range(len(dict_color)):
        avg_brightness += sum(dict_color[i]['RGB Color']) / len(dict_color) / 3
        avg_r += dict_color[i]['RGB Color'][0] / len(dict_color)
        avg_g += dict_color[i]['RGB Color'][1] / len(dict_color)
        avg_b += dict_color[i]['RGB Color'][2] / len(dict_color)

    feature_list[FIELD_COLOR_BRIGHTNESS] = int(avg_brightness)
    feature_list[FIELD_COLOR_WORMTH] = round(max(avg_r, avg_g, avg_b) / 255 * 100, 2)
    if dict_color[0]['Percentage'] >= 30:
        feature_list[FIELD_IS_DOMINANT_COLOR] = 'True'
        feature_list[FIELD_DOMINANT_COLOR] = dict_color[0]['Hex Color']
    else:
        feature_list[FIELD_IS_DOMINANT_COLOR] = 'False'
        feature_list[FIELD_DOMINANT_COLOR] = ''

    # --------------------------- Face features from Google -----------------------------
    feature_list[FIELD_IS_3S_FACE] = 'False'
    feature_list[FIELD_3S_FACES] = 0
    feature_list[FIELD_IS_10S_FACE] = 'False'
    feature_list[FIELD_10S_FACES] = 0
    if 'face_detection_annotations' in dict_general_google:
        face_info = dict_general_google['face_detection_annotations']
        for i in range(len(face_info)):
            start_time = face_info[i]['tracks'][0]['segment']['start_time_offset']
            end_time = face_info[i]['tracks'][0]['segment']['end_time_offset']
            if 'seconds' not in start_time:
                st = 0
            else:
                st = start_time['seconds']

            if 'seconds' not in end_time:
                et = 100000
            else:
                et = end_time['seconds']

            if st < 3 and et - st > 1:
                feature_list[FIELD_3S_FACES] += 1
                feature_list[FIELD_IS_3S_FACE] = 'True'

            if (st < 10 or et > 3) and et - st > 1:
                feature_list[FIELD_10S_FACES] += 1
                feature_list[FIELD_IS_10S_FACE] = 'True'

    # --------------------------- Face features (Age, Gender) ----------------------------
    feature_list[FIELD_3S_AGE], feature_list[FIELD_3S_GENDER] = \
        avg_gender_age(1, min(len(dict_face.keys()) + 1, 60))

    feature_list[FIELD_10S_AGE], feature_list[FIELD_10S_GENDER] = \
        avg_gender_age(min(len(dict_face.keys()) + 1, 60), min(len(dict_face.keys()) + 1, 200))

    # ---------------------------- OCR features from Google ------------------------------
    feature_list[FIELD_3S_TEXT_WORDS] = 0
    feature_list[FIELD_10S_TEXT_WORDS] = 0
    feature_list[FIELD_3S_TEXT_WORDS_MOOD] = 'No'
    feature_list[FIELD_10S_TEXT_WORDS_MOOD] = 'No'
    if 'text_annotations' in dict_general_google:
        ocr_info = dict_general_google['text_annotations']
        for i in range(len(ocr_info)):
            start_time = ocr_info[i]['segments'][0]['segment']['start_time_offset']
            end_time = ocr_info[i]['segments'][0]['segment']['end_time_offset']
            if 'seconds' not in start_time:
                st = 0
            else:
                st = start_time['seconds']

            if 'seconds' not in end_time:
                et = 100000
            else:
                et = end_time['seconds']

            if st < 3:
                feature_list[FIELD_3S_TEXT_WORDS] += 1
                feature_list[FIELD_3S_TEXT_WORDS_MOOD] = 'Yes'

            if 3 <= st < 10 or 3 <= et < 10:
                feature_list[FIELD_10S_TEXT_WORDS] += 1
                feature_list[FIELD_10S_TEXT_WORDS_MOOD] = 'Yes'

    # ---------------------------- Voice features from Google ----------------------------
    feature_list[FIELD_3S_VOICE_WORDS] = 0
    feature_list[FIELD_10S_VOICE_WORDS] = 0
    feature_list[FIELD_3S_VOICE_WORDS_MOOD] = 'No'
    feature_list[FIELD_10S_VOICE_WORDS_MOOD] = 'No'
    if 'speech_transcriptions' in dict_speech_google:
        speech_info = dict_speech_google['speech_transcriptions']
        for i in range(len(speech_info)):
            if "words" not in speech_info[i]['alternatives'][0]:
                continue

            for word_ind in range(len(speech_info[i]['alternatives'][0]["words"])):
                start_time = speech_info[i]['alternatives'][0]["words"][word_ind]['start_time']
                end_time = speech_info[i]['alternatives'][0]["words"][word_ind]['end_time']
                if 'seconds' not in start_time:
                    st = 0
                else:
                    st = start_time['seconds']

                if 'seconds' not in end_time:
                    et = 100000
                else:
                    et = end_time['seconds']

                if st < 3:
                    feature_list[FIELD_3S_VOICE_WORDS] += 1
                    feature_list[FIELD_3S_VOICE_WORDS_MOOD] = 'Yes'

                if 3 <= st < 10 or 3 <= et < 10:
                    feature_list[FIELD_10S_VOICE_WORDS] += 1
                    feature_list[FIELD_10S_VOICE_WORDS_MOOD] = 'Yes'

    # ---------------------------------- Other features -----------------------------------
    if 'video_info' in json_video:
        video_info = json_video['video_info']
        ratio = video_info['height'] / video_info['width']

        if ratio < 0.9:
            feature_list[FIELD_PERF_DIM_RATIO] = VIDEO_ASPECT_RATIO_HORIZONTAL
        elif ratio < 1.1:
            feature_list[FIELD_PERF_DIM_RATIO] = VIDEO_ASPECT_RATIO_SQUARE
        else:
            feature_list[FIELD_PERF_DIM_RATIO] = VIDEO_ASPECT_RATIO_VERTICAL

        feature_list[FIELD_PERF_VIDEO_DURATION] = round(video_info['duration'] / 15) * 15

    if 'audio_info' in json_video:
        feature_list[FIELD_AUDIO_MOOD] = json_video['audio_info']['mood']

    return feature_list


def save_features(output_file, video_feature_list, video_filename_list, reference_csv):
    key_features = [FIELD_PERF_PLATFORM_ID, FIELD_PERF_DIM_RATIO, FIELD_PERF_VIDEO_DURATION, FIELD_COLOR_BRIGHTNESS,
                    FIELD_COLOR_WORMTH, FIELD_IS_DOMINANT_COLOR, FIELD_DOMINANT_COLOR, FIELD_IS_3S_FACE,
                    FIELD_3S_GENDER, FIELD_3S_AGE, FIELD_3S_FACES, FIELD_IS_10S_FACE, FIELD_10S_GENDER, FIELD_10S_AGE,
                    FIELD_10S_FACES, FIELD_3S_VOICE_WORDS, FIELD_10S_VOICE_WORDS, FIELD_3S_VOICE_WORDS_MOOD,
                    FIELD_10S_VOICE_WORDS_MOOD, FIELD_3S_TEXT_WORDS, FIELD_10S_TEXT_WORDS, FIELD_3S_TEXT_WORDS_MOOD,
                    FIELD_10S_TEXT_WORDS_MOOD, FIELD_AUDIO_MOOD, FIELD_SCORE]

    # ------------------ Read reference csv and store data ----------------------
    ref_data = func.read_csv(reference_csv)
    key_ref_index_list = [-1, -1, -1, -1, -1, -1, -1, -1, -1]
    if len(ref_data) > 0:
        # find mediaID index
        for col_ind in range(len(ref_data[0])):
            for key_ref_ind in range(len(KEY_REF_CSV)):
                if ref_data[0][col_ind].lower() == KEY_REF_CSV[key_ref_ind].lower():
                    key_ref_index_list[key_ref_ind] = col_ind

    # ---------------------------- Create csv dataset ----------------------------
    output_csv_content = [['filename'] + KEY_REF_CSV + key_features]
    for video_ind in range(len(video_filename_list)):
        value_ref = [''] * len(KEY_REF_CSV)
        video_info = []

        # find matched video info from reference csv data
        if len(ref_data) > 0:
            media_id_index = key_ref_index_list[0]
            if media_id_index >= 0:
                for i in range(1, len(ref_data)):
                    media_id = ref_data[i][media_id_index]
                    if media_id in video_filename_list[video_ind]:
                        video_info = ref_data[i]
                        break

        if video_info:
            for i in range(len(KEY_REF_CSV)):
                value_ref[i] = video_info[key_ref_index_list[i]]

        # save features to csv
        value_features = []
        for i in range(len(key_features)):
            if key_features[i] in video_feature_list[video_ind]:
                value_features.append(str(video_feature_list[video_ind][key_features[i]]))
            else:
                value_features.append('')

        output_csv_content.append([video_filename_list[video_ind]] + value_ref + value_features)

    # -------------------------- Write to csv file -----------------------------
    func.save_csv(output_file, output_csv_content)


if __name__ == '__main__':
    # in_arg = ['../video/5_wy3l3n08.mp4', '../document/creative-analysis-1481009631.csv']
    in_arg = ['../video', '../document/creative-analysis-1481009631.csv']
    # in_arg = ['../video/5.mp4', '']

    for arg_ind in range(1, len(sys.argv)):
        in_arg[arg_ind - 1] = sys.argv[arg_ind]

    # ------------------------- Check input argument ----------------------------
    video_source_path = in_arg[0]
    ref_csv_filename = in_arg[1]

    video_list = []
    if os.path.isfile(video_source_path):
        video_list = [video_source_path]
    elif os.path.isdir(video_source_path):
        _, _, total_file_path = func.get_file_list(video_source_path)
        for file_path in total_file_path:
            if any(file_path.lower().endswith(s) for s in ['.mp4', 'avi', '.mpg', '.mpeg']):
                video_list.append(file_path)
    else:
        exit(0)

    # -------------------- Analysis video and extract features --------------------
    vid_feature_list = []
    for vid_ind in range(len(video_list)):
        print(f'Processing {video_list[vid_ind]} ({vid_ind + 1}/{len(video_list)}) ...')
        json_all = analysis_video(video_list[vid_ind])
        # json_all = func.read_json(OUTPUT_JSON_ALL)

        feature = extract_features(json_all)
        # print(json.dumps(feature, indent=4))

        vid_feature_list.append(feature)

    # -------------------------- Save features to csv file ------------------------
    output_csv_file = 'output_feature.csv'
    save_features(output_file=output_csv_file,
                  video_feature_list=vid_feature_list,
                  video_filename_list=video_list,
                  reference_csv=ref_csv_filename)

    # -------------------------- Generate upgrade csv file -----------------------
    output_upgrade_csv_file = 'output_upgraded_feature.csv'
    convert_advanced_csv(output_csv_file, output_upgrade_csv_file)
