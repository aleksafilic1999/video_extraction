#!/usr/bin/python

from math import tanh
import time
import sys
import datetime
import pygad
import numpy


def expression(inputs):
    if type(inputs) != list:
        print('Argument must be a list')
        exit()
    if len(inputs) != 20:
        print('Incorrect number of inputs')
        exit()
    Ext_colorBrightness = inputs[0]
    Ext_colorWormth = inputs[1]
    Ext_3SecondFaces = inputs[2]
    Ext_3to10SecondFaces = inputs[3]
    Ext_3SecondDominantFaceAgeGroup = inputs[4]
    Ext_3SecondDominantFaceGender = inputs[5]
    Ext_3to10SecondDominantFaceAgeGroup = inputs[6]
    Ext_3to10SecondDominantFaceGender = inputs[7]
    Ext_numOCRWords3Sec = inputs[8]
    Ext_numOCRWords3to10Sec = inputs[9]
    Ext_OCRWords3SecMood = inputs[10]
    Ext_numOCRWords3to10SecMood = inputs[11]
    Ext_numTranscriptionWords3Sec = inputs[12]
    Ext_numTranscriptionWords3to10Sec = inputs[13]
    Ext_TranscriptionWords3SecMood = inputs[14]
    Ext_numTranscriptionWords3to10SecMood = inputs[15]
    category = inputs[16]
    horizontal = inputs[17]
    vertical = inputs[18]
    square = inputs[19]
    scaled_Ext_colorBrightness = 2 * (Ext_colorBrightness - 1) / (4 - 1) - 1
    scaled_Ext_colorWormth = 2 * (Ext_colorWormth - 1) / (4 - 1) - 1
    scaled_Ext_3SecondFaces = (Ext_3SecondFaces - 0.449295) / 0.688672
    scaled_Ext_3to10SecondFaces = (Ext_3to10SecondFaces - 0.916667) / 0.92059
    scaled_Ext_3SecondDominantFaceAgeGroup = (Ext_3SecondDominantFaceAgeGroup - 1.45899) / 1.42372
    scaled_Ext_3SecondDominantFaceGender = (Ext_3SecondDominantFaceGender - 0.836861) / 0.817659
    scaled_Ext_3to10SecondDominantFaceAgeGroup = (Ext_3to10SecondDominantFaceAgeGroup - 1.93651) / 1.37865
    scaled_Ext_3to10SecondDominantFaceGender = (Ext_3to10SecondDominantFaceGender - 1.03483) / 0.755584
    scaled_Ext_numOCRWords3Sec = (Ext_numOCRWords3Sec - 0.787919) / 0.727853
    scaled_Ext_numOCRWords3to10Sec = (Ext_numOCRWords3to10Sec - 1.09612) / 0.8997
    scaled_Ext_OCRWords3SecMood = (Ext_OCRWords3SecMood - 0.606702) / 0.48859
    scaled_Ext_numOCRWords3to10SecMood = (Ext_numOCRWords3to10SecMood - 0.638889) / 0.480429
    scaled_Ext_numTranscriptionWords3Sec = (Ext_numTranscriptionWords3Sec - 0.222663) / 0.416126
    scaled_Ext_numTranscriptionWords3to10Sec = (Ext_numTranscriptionWords3to10Sec - 0.283069) / 0.450589
    scaled_Ext_TranscriptionWords3SecMood = (Ext_TranscriptionWords3SecMood - 0.222663) / 0.416126
    scaled_Ext_numTranscriptionWords3to10SecMood = (Ext_numTranscriptionWords3to10SecMood - 0.283069) / 0.450589
    scaled_category = 2 * (category - 0) / (1 - 0) - 1
    scaled_horizontal = (horizontal - 0.188272) / 0.391015
    scaled_vertical = 2 * (vertical - 0) / (1 - 0) - 1
    scaled_square = (square - 0.138007) / 0.344983
    y_1_1 = tanh(-4.83444 + (scaled_Ext_colorBrightness * -1.19196) + (scaled_Ext_colorWormth * -2.62733) + (
                scaled_Ext_3SecondFaces * -7.47806) + (scaled_Ext_3to10SecondFaces * -1.20829) + (
                             scaled_Ext_3SecondDominantFaceAgeGroup * -2.86365) + (
                             scaled_Ext_3SecondDominantFaceGender * 0.217321) + (
                             scaled_Ext_3to10SecondDominantFaceAgeGroup * -0.864108) + (
                             scaled_Ext_3to10SecondDominantFaceGender * 0.678134) + (
                             scaled_Ext_numOCRWords3Sec * -2.22548) + (scaled_Ext_numOCRWords3to10Sec * 2.66794) + (
                             scaled_Ext_OCRWords3SecMood * 0.0773143) + (
                             scaled_Ext_numOCRWords3to10SecMood * -5.33012) + (
                             scaled_Ext_numTranscriptionWords3Sec * 1.9675) + (
                             scaled_Ext_numTranscriptionWords3to10Sec * 0.979656) + (
                             scaled_Ext_TranscriptionWords3SecMood * 1.40252) + (
                             scaled_Ext_numTranscriptionWords3to10SecMood * 0.823832) + (scaled_category * -1.28966) + (
                             scaled_horizontal * -0.0287059) + (scaled_vertical * -0.948094) + (
                             scaled_square * -0.506029))
    y_1_2 = tanh(-0.682424 + (scaled_Ext_colorBrightness * -2.71331) + (scaled_Ext_colorWormth * -2.55288) + (
                scaled_Ext_3SecondFaces * 1.62085) + (scaled_Ext_3to10SecondFaces * 2.89443) + (
                             scaled_Ext_3SecondDominantFaceAgeGroup * 1.14252) + (
                             scaled_Ext_3SecondDominantFaceGender * -1.9018) + (
                             scaled_Ext_3to10SecondDominantFaceAgeGroup * -2.73859) + (
                             scaled_Ext_3to10SecondDominantFaceGender * 3.4712) + (
                             scaled_Ext_numOCRWords3Sec * -2.21798) + (scaled_Ext_numOCRWords3to10Sec * -0.0285643) + (
                             scaled_Ext_OCRWords3SecMood * -0.740762) + (
                             scaled_Ext_numOCRWords3to10SecMood * -1.98194) + (
                             scaled_Ext_numTranscriptionWords3Sec * 3.80328) + (
                             scaled_Ext_numTranscriptionWords3to10Sec * -0.651107) + (
                             scaled_Ext_TranscriptionWords3SecMood * 3.70136) + (
                             scaled_Ext_numTranscriptionWords3to10SecMood * -0.417326) + (
                             scaled_category * -2.07066) + (scaled_horizontal * -1.16237) + (
                             scaled_vertical * 2.59125) + (scaled_square * -1.93609))
    y_1_3 = tanh(5.4169 + (scaled_Ext_colorBrightness * 0.866196) + (scaled_Ext_colorWormth * 1.26905) + (
                scaled_Ext_3SecondFaces * -6.8302) + (scaled_Ext_3to10SecondFaces * 1.2737) + (
                             scaled_Ext_3SecondDominantFaceAgeGroup * -0.32896) + (
                             scaled_Ext_3SecondDominantFaceGender * -1.88584) + (
                             scaled_Ext_3to10SecondDominantFaceAgeGroup * 0.158991) + (
                             scaled_Ext_3to10SecondDominantFaceGender * -1.9512) + (
                             scaled_Ext_numOCRWords3Sec * -6.09256) + (scaled_Ext_numOCRWords3to10Sec * -0.724007) + (
                             scaled_Ext_OCRWords3SecMood * 0.923677) + (
                             scaled_Ext_numOCRWords3to10SecMood * -1.4337) + (
                             scaled_Ext_numTranscriptionWords3Sec * -1.88307) + (
                             scaled_Ext_numTranscriptionWords3to10Sec * 0.340063) + (
                             scaled_Ext_TranscriptionWords3SecMood * -1.12932) + (
                             scaled_Ext_numTranscriptionWords3to10SecMood * 0.825371) + (scaled_category * -1.43241) + (
                             scaled_horizontal * 0.0495855) + (scaled_vertical * 1.33979) + (scaled_square * -0.598219))
    scaled_score = (-0.261819 + (y_1_1 * 0.335524) + (y_1_2 * -0.207591) + (y_1_3 * -0.23709))
    score = (0.5 * (scaled_score + 1.0) * (24 - 0) + 0)

    return score


def fitness_func(solution, solution_idx):
    inputvals = [solution[0], solution[1], solution[2], solution[3], solution[4], solution[5], solution[6], solution[7],
                 solution[8], solution[9], solution[10], solution[11], solution[12], solution[13], solution[14],
                 solution[15], solution[16], solution[17], solution[18], solution[19]]
    score = expression(inputvals)
    return score


if __name__ == '__main__':
    fitness_function = fitness_func

    num_generations = 5000
    num_parents_mating = 100

    sol_per_pop = 500
    num_genes = 21

    init_range_low = -100
    init_range_high = 100

    parent_selection_type = "sss"
    keep_parents = 1

    crossover_type = "single_point"

    mutation_type = "random"
    mutation_percent_genes = 20

    ga_instance = pygad.GA(num_generations=num_generations,
                           num_parents_mating=num_parents_mating,
                           fitness_func=fitness_function,
                           sol_per_pop=sol_per_pop,
                           num_genes=num_genes,
                           init_range_low=init_range_low,
                           init_range_high=init_range_high,
                           parent_selection_type=parent_selection_type,
                           keep_parents=keep_parents,
                           crossover_type=crossover_type,
                           mutation_type=mutation_type,
                           mutation_percent_genes=mutation_percent_genes,
                           gene_type=int)

    starttime = datetime.datetime.now()
    ga_instance.run()
    endtime = datetime.datetime.now()
    timediff = endtime - starttime
    print(timediff)
    solution, solution_fitness, solution_idx = ga_instance.best_solution()
    print("Parameters of the best solution : {solution}".format(solution=solution))
    print("Fitness value of the best solution = {solution_fitness}".format(solution_fitness=solution_fitness))

    prediction = solution
    print("Predicted output based on the best solution : {prediction}".format(prediction=prediction))

    ga_instance.plot_fitness()
    # ga_instance.plot_fitness(plot_type="plot")
