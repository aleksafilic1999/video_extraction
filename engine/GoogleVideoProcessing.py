from google.cloud import videointelligence
from google.cloud import storage
from datetime import datetime
from engine.constant import *
import sys


class GoogleVideoProcessing:

    def __init__(self):
        storage_client = storage.Client.from_service_account_json(GOOGLE_CREDENTIAL_FILE, project=GOOGLE_PROJECT_NAME)
        self.storage_bucket = storage_client.get_bucket(GOOGLE_STORAGE_BUCKET_NAME)

        self.video_client = \
            videointelligence.VideoIntelligenceServiceClient.from_service_account_file(GOOGLE_CREDENTIAL_FILE)

    def __upload_storage__(self, local_file_path, cloud_file_path):
        blob = self.storage_bucket.blob(cloud_file_path)
        blob.upload_from_filename(local_file_path)

    def __analyze_video__(self, cloud_file_path, result_json_path):
        gcs_uri = f"gs://{GOOGLE_STORAGE_BUCKET_NAME}/{cloud_file_path}"
        output_uri = f"gs://{GOOGLE_STORAGE_BUCKET_NAME}/{result_json_path}"

        features = [
            videointelligence.Feature.OBJECT_TRACKING,
            # videointelligence.Feature.LABEL_DETECTION,
            # videointelligence.Feature.SHOT_CHANGE_DETECTION,
            videointelligence.Feature.SPEECH_TRANSCRIPTION,
            videointelligence.Feature.LOGO_RECOGNITION,
            # videointelligence.Feature.EXPLICIT_CONTENT_DETECTION,
            videointelligence.Feature.TEXT_DETECTION,
            videointelligence.Feature.FACE_DETECTION,
            videointelligence.Feature.PERSON_DETECTION
        ]

        transcript_config = videointelligence.SpeechTranscriptionConfig(language_code="en-US",
                                                                        enable_automatic_punctuation=True)

        person_config = videointelligence.PersonDetectionConfig(include_bounding_boxes=True,
                                                                include_attributes=False,
                                                                include_pose_landmarks=True)

        face_config = videointelligence.FaceDetectionConfig(include_bounding_boxes=True, include_attributes=True)

        video_context = videointelligence.VideoContext(speech_transcription_config=transcript_config,
                                                       person_detection_config=person_config,
                                                       face_detection_config=face_config)

        request = {"features": features,
                   "input_uri": gcs_uri,
                   "output_uri": output_uri,
                   "video_context": video_context}

        operation = self.video_client.annotate_video(request=request)

        result = operation.result(timeout=600)
        # print(result)

        return result

    def __download_storage__(self, cloud_file_path, local_file_path):
        blob = self.storage_bucket.blob(cloud_file_path)
        blob.download_to_filename(local_file_path)

    def process_video(self, video_filename, output_json_filename):
        dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        temp_cloud_video = f'{CLOUD_VIDEO_UPLOAD_FOLDER}/{dt_string}.mp4'
        temp_cloud_json = f'{CLOUD_ANAL_JSON_FOLDER}/{dt_string}.json'

        print("\t\tUploading video to the Google cloud storage ...")
        self.__upload_storage__(local_file_path=video_filename, cloud_file_path=temp_cloud_video)

        print("\t\tStarting to process video ...")
        self.__analyze_video__(cloud_file_path=temp_cloud_video, result_json_path=temp_cloud_json)

        print("\t\tFinished processing, downloading result json file ...")
        self.__download_storage__(cloud_file_path=temp_cloud_json, local_file_path=output_json_filename)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        filename = '../../video/5.mp4'

    class_google_video = GoogleVideoProcessing()
    class_google_video.process_video(filename, 'output_google.json')
