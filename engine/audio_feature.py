from scipy.signal import find_peaks
from pydub import AudioSegment
from engine.func import upgrade_float_list
from engine.constant import *
import statsmodels.api as sm
import moviepy.editor as mp
import numpy as np
import librosa


VERY_LOW = 0
LOW = 1
MEDIUM = 2
HIGH = 3
VERY_HIGH = 4


def convert_video_to_audio(in_video_file, out_audio_file):
    clip = mp.VideoFileClip(in_video_file)
    if clip.audio is None:
        return False
    else:
        clip.audio.write_audiofile(out_audio_file)
        return True


def audio_feature_intensity(in_audio_file):
    audio_segment = AudioSegment.from_file(file=in_audio_file)
    audio_channels = audio_segment.channels
    audio_sample_width = audio_segment.sample_width
    audio_sample_rate = audio_segment.frame_rate
    audio_intensity = audio_segment.dBFS
    # print(f"Frame width: {audio_segment.frame_width}")
    # print(f"Length (ms): {len(audio_segment)}")
    # print(f"Frame count: {audio_segment.frame_count()}")
    return abs(audio_intensity) / 100, audio_channels, audio_sample_width, audio_sample_rate


def audio_feature_pitch(in_audio_file):
    # Load data and sampling frequency from the data file
    data, sampling_frequency = librosa.load(in_audio_file)

    auto = sm.tsa.acf(data, nlags=2000)

    peaks = find_peaks(auto)[0]     # Find peaks of the auto-correlation
    lag = peaks[0]      # Choose the first peak as our pitch component lag

    pitch = sampling_frequency / lag  # Transform lag into frequency

    return pitch


def audio_feature_rhythm(in_audio_file):
    x, sr = librosa.load(in_audio_file)
    onset_frames = librosa.onset.onset_detect(x, sr=sr, wait=1, pre_avg=1, post_avg=1, pre_max=1, post_max=1)
    onset_times = librosa.frames_to_time(onset_frames)
    # remove extension, .mp3, .wav etc.
    return (onset_times[-1] - onset_frames[0]) / len(onset_times) * 1000


def audio_feature_timbre(in_audio_file):
    x, sr = librosa.load(in_audio_file)
    xdb = librosa.amplitude_to_db(abs(librosa.stft(x)))
    return abs(np.mean(xdb)) / 100


def get_audio_feature(in_audio_file):
    feature_intensity, audio_channels, audio_sample_width, audio_sample_rate = audio_feature_intensity(in_audio_file)
    feature_timbre = audio_feature_timbre(in_audio_file)
    feature_pitch = audio_feature_pitch(in_audio_file)
    feature_rhythm = audio_feature_rhythm(in_audio_file)
    # print(feature_intensity, feature_timbre, feature_pitch, feature_rhythm)

    val_list = [VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH]
    val_intensity = upgrade_float_list(feature_intensity, [0.1, 0.2, 0.3, 0.4], val_list)
    val_timbre = upgrade_float_list(feature_timbre, [0.05, 0.1, 0.15, 0.2], val_list)
    val_pitch = upgrade_float_list(feature_pitch, [5000, 10000, 15000, 20000], val_list)
    val_rhythm = upgrade_float_list(feature_rhythm, [100, 200, 300, 400], val_list)

    if val_intensity == val_timbre == MEDIUM and val_pitch == val_rhythm == VERY_HIGH:
        ret_mood = AUDIO_MOOD_HAPPY
    elif val_timbre == MEDIUM and val_intensity == val_pitch == val_rhythm == HIGH:
        ret_mood = AUDIO_MOOD_EXUBERANT
    elif val_intensity == VERY_HIGH and val_pitch == val_timbre == MEDIUM and val_rhythm == HIGH:
        ret_mood = AUDIO_MOOD_ENERGETIC
    elif val_intensity == HIGH and val_timbre == val_rhythm == VERY_HIGH and val_pitch == VERY_LOW:
        ret_mood = AUDIO_MOOD_FRANTIC
    elif val_intensity == MEDIUM and val_timbre == val_pitch == VERY_LOW and val_rhythm == LOW:
        ret_mood = AUDIO_MOOD_ANXIOUS
    elif val_intensity == val_timbre == val_pitch == val_rhythm == LOW:
        ret_mood = AUDIO_MOOD_DEPRESSION
    elif val_intensity == val_timbre == val_rhythm == VERY_LOW and val_pitch == MEDIUM:
        ret_mood = AUDIO_MOOD_CALM
    elif val_intensity == val_timbre == val_rhythm == LOW and val_pitch == HIGH:
        ret_mood = AUDIO_MOOD_CONTENTMENT
    else:
        ret_mood = AUDIO_MOOD_HAPPY

    return ret_mood, audio_channels, audio_sample_width, audio_sample_rate


if __name__ == '__main__':
    video_file = '../../video/5_wy3l3n08.mp4'
    audio_file = "result.mp3"

    convert_video_to_audio(in_video_file=video_file, out_audio_file=audio_file)
    print(get_audio_feature(audio_file))
