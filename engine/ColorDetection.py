from engine.constant import *
from engine import func
import scipy.cluster
import numpy as np
import binascii
import json
import cv2


class ColorDetection:

    def __init__(self):
        pass

    @staticmethod
    def detect_color_image(img):
        # img = cv2.resize(img, (150, 150))
        ar = np.asarray(img)
        shape = ar.shape
        ar = ar.reshape(np.product(shape[:2]), shape[2]).astype(float)

        codes, dist = scipy.cluster.vq.kmeans(ar, COLOR_DET_NUM_CLUSTERS)

        v, _ = scipy.cluster.vq.vq(ar, codes)  # assign codes
        counts, bins = np.histogram(v, len(codes))  # count occurrences

        color_info_list = []
        for i in range(len(counts)):
            peak = [int(x) for x in codes[i]]
            color = binascii.hexlify(bytearray(peak)).decode('ascii')
            color_info_list.append([peak, color, counts[i] / sum(counts) * 100])

        color_info_list = sorted(color_info_list, key=lambda x: x[2], reverse=True)

        result = []
        for i in range(COLOR_DET_DISPLAY_COUNTS):
            result.append({'RGB Color': color_info_list[i][0],
                           'Hex Color': color_info_list[i][1],
                           'Percentage': color_info_list[i][2]})

        return {'most_color_detection': result}

    def detect_color_video(self, video_file):
        cap = cv2.VideoCapture(video_file)
        video_fps = cap.get(cv2.CAP_PROP_FPS)
        video_frame_counts = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        video_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        video_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

        check_frame_count = min(int(video_fps * COLOR_DET_CHECK_DURATION), int(video_frame_counts))
        new_img = None
        for i in range(check_frame_count):
            _, frame = cap.read()
            img = cv2.resize(frame, (50, 50))
            if i == 0:
                new_img = img
            else:
                new_img = np.concatenate((new_img, img), axis=1)

        # cv2.imshow('d', new_img)
        # cv2.waitKey(0)
        return self.detect_color_image(new_img), (video_width, video_height, video_frame_counts / video_fps)


if __name__ == '__main__':
    class_color = ColorDetection()
    # im = cv2.imread('../video/1.jpg')
    # ret_color = class_color.detect_color_image(im)
    # print(ret_color)

    vid_file = '../../video/1.mp4'
    ret_color = class_color.detect_color_video(vid_file)
    func.write_text('output_color.json', json.dumps(ret_color, indent=4))

    print(ret_color)
