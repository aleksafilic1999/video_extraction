import cv2


class FaceProcess:

    def __init__(self, face_engine='dnn'):
        if face_engine == 'dlib':
            from engine.face_engine_dlib import FaceEngineDlib
            self.class_face = FaceEngineDlib()
        elif face_engine == 'dnn':
            from engine.face_engine_dnn import FaceEngineDNN
            self.class_face = FaceEngineDNN()
        else:
            from engine.face_engine_dnn import FaceEngineDNN
            self.class_face = FaceEngineDNN()

    def process_video(self, vid_name, scale_factor=1.0, show_video=True, write_video=False):
        cap = cv2.VideoCapture(vid_name)
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH) * scale_factor)
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT) * scale_factor)
        fps = cap.get(cv2.CAP_PROP_FPS)
        if write_video:
            vid_output = cv2.VideoWriter("output.mp4", cv2.VideoWriter_fourcc(*'MPEG'), fps, (width, height))

        frame_ind = 0
        result_json = {'face_detection': {}}

        while True:
            # -------------------- read frames ---------------------
            frame_ind += 1
            ret, frame = cap.read()
            if not ret:
                break

            if scale_factor != 1:
                frame = cv2.resize(frame, None, fx=scale_factor, fy=scale_factor)

            # --------------------- detect faces ---------------------
            _, face_list, coordinate_list = self.class_face.get_faces(frame)

            # ------------------- detect age/gender ------------------
            age_gender_list = self.class_face.predict_age_and_gender(face_list)

            # ------------------- create json result -----------------
            frame_result = []
            for i in range(len(coordinate_list)):
                frame_result.append({
                    'face': str(list(coordinate_list[i])),
                    'age': age_gender_list[i][0],
                    'gender': age_gender_list[i][1]
                })
            result_json['face_detection'][f'frame{frame_ind}'] = frame_result

            # --------------------- show results ---------------------
            if show_video:
                for i in range(len(coordinate_list)):
                    box_color = (255, 0, 0) if age_gender_list[i][1] == "Male" else (147, 20, 255)
                    cv2.rectangle(frame, tuple(coordinate_list[i][:2]), tuple(coordinate_list[i][2:]), box_color, 2)
                    # Label processed image
                    label = f'{age_gender_list[i][0]}, {age_gender_list[i][1]}'
                    cv2.putText(frame, label, (coordinate_list[i][0], coordinate_list[i][1] - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.54, box_color, 2)

                cv2.imshow('video', frame)
                if write_video:
                    vid_output.write(frame)
                if cv2.waitKey(5) == ord('q'):
                    break

        cap.release()
        if write_video:
            vid_output.release()

        return result_json
