import numpy as np
import base64
import shutil
import json
import csv
import os


def rm_file(file_name):
    if os.path.isfile(file_name):
        os.remove(file_name)


def rm_tree(folder_name):
    shutil.rmtree(folder_name, True)
    os.mkdir(folder_name)


def img_encode_b64(img_name):
    img_file = open(img_name, 'rb')
    img_b64_data = base64.b64encode(img_file.read()).decode('UTF-8')
    return img_b64_data


def img_decode_b64(img_date, img_name):
    img_out = open(img_name, 'wb')
    img_out.write(img_date.decode('base64'))
    img_out.close()


def get_distance(vec1, vec2):
    d = 0
    for i in range(len(vec1)):
        d += (float(vec1[i]) - float(vec2[i])) ** 2

    if d < 0.4:
        dist = d / 6
    else:
        dist = d * (6.0 / 7) + 1.0 / 7

    return dist


def get_match_value(vec1, vec2):
    distance = get_distance(vec1, vec2)
    return 100 * (max(1 - distance, 0.0))


def write_text(filename, text):
    file1 = open(filename, 'w')
    file1.write(text)
    file1.close()


def read_text(filename):
    file1 = open(filename, 'r')
    text = file1.read()
    file1.close()

    return text


def get_file_list(root_dir):
    path_list = []
    file_list = []
    join_list = []
    for path, _, files in os.walk(root_dir):
        for name in files:
            path_list.append(path)
            file_list.append(name)
            join_list.append(os.path.join(path, name))

    return path_list, file_list, join_list


def read_csv(filename):
    """
        load the csv data and return it.
    """
    if not os.path.isfile(filename):
        return []

    file_csv = open(filename, 'r')
    reader = csv.reader(file_csv)
    data_csv = []
    for row_data in reader:
        data_csv.append(row_data)

    file_csv.close()
    return data_csv


def save_csv(filename, data):
    """
        save the "data" to filename as csv format.
    """
    file_out = open(filename, 'w', newline='')
    writer = csv.writer(file_out)
    writer.writerows(data)
    file_out.close()


def append_csv(filename, data):
    file_out = open(filename, 'a')
    writer = csv.writer(file_out)
    writer.writerows(data)
    file_out.close()


def read_json(filename):
    text = read_text(filename)
    return json.loads(text)


def get_emotion_features(lm):
    land_rot = np.rot90(lm)

    norm_x = normalizing_vector(land_rot[0], 100)
    norm_y = normalizing_vector(land_rot[1], 100)

    return norm_x + norm_y


def normalizing_vector(vec, d_max):
    min_vec = min(vec)
    max_vec = max(vec)
    a = float(d_max) / (max_vec-min_vec)
    b = float(d_max) * min_vec / (min_vec-max_vec)
    new_vec = []
    for i in range(len(vec)):
        new_val = a * vec[i] + b
        new_vec.append(int(new_val))

    return new_vec


def get_rect_distance(rect1, rect2):
    cx1, cy1 = (rect1[0] + rect1[2]) / 2, (rect1[1] + rect1[3]) / 2
    cx2, cy2 = (rect2[0] + rect2[2]) / 2, (rect2[1] + rect2[3]) / 2

    return int(np.sqrt((cx1 - cx2) ** 2 + (cy1 - cy2) ** 2))


def points_distance(pt1, pt2):
    return np.sqrt((pt2[0] - pt1[0]) ** 2 + (pt2[1] - pt1[1]) ** 2)


def check_same_rect(rect1, rect2):
    w = max(rect1[2] - rect1[0], rect2[2] - rect2[0])
    h = max(rect1[3] - rect1[1], rect2[3] - rect2[1])

    if all([4 * abs(rect1[0] - rect2[0]) < w,
            4 * abs(rect1[2] - rect2[2]) < w,
            4 * abs(rect1[1] - rect2[1]) < h,
            4 * abs(rect1[3] - rect2[3]) < h]):
        return True

    else:
        return False


def is_float(a):
    try:
        float(a)
        return True
    except ValueError:
        return False


def upgrade_float_list(in_val, range_list, value_list):
    """
        range_list = [10, 20], value_list = [3, 1, 2] =>
        output  3: in_val < 10
                1: in_val < 20
                2: else
    """
    if len(range_list) + 1 != len(value_list):
        return 0
    elif is_float(in_val):
        val = float(in_val)
        for i in range(len(range_list)):
            if val <= range_list[i]:
                return value_list[i]

        return value_list[-1]
    else:
        return 0


def upgrade_string_list(in_val, string_list, value_list):
    """
        range_list = ['Game', 'Text'], value_list = [3, 1, 2] =>
        output  3: in_val == 'Game'
                1: in_val == 'Text'
                2: else
    """
    if len(string_list) + 1 != len(value_list):
        return 0
    else:
        for i in range(len(string_list)):
            if in_val == string_list[i]:
                return value_list[i]

        return value_list[-1]
