from engine.wide_resnet import WideResNet
import numpy as np
import dlib
import cv2


class FaceEngineDlib:

    def __init__(self):
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor('models/dlib_model.dat')

        self.age_gender_img_size = 64
        self.age_gender_model = WideResNet(self.age_gender_img_size, depth=16, k=8)()
        self.age_gender_model.load_weights('models/weights.18-4.06.hdf5')

    def get_landmarks(self, im, rects):
        points_list = []

        if len(rects) != 0:
            for i in range(len(rects)):
                predict_ret = self.predictor(im, rects[i]).parts()
                points = []
                for p in predict_ret:
                    points.append((p.x, p.y))

                points_list.append(points)

            return points_list

        else:
            return []

    def __get_face_rect(self, im, mode=0):
        rect = self.detector(im, mode)
        return rect

    def get_faces(self, im, mode=0):
        face_list = []
        coordinate_list = []
        rect = self.__get_face_rect(im, mode=mode)

        for i, d in enumerate(rect):
            x1, y1 = d.left(), d.top()
            x2, y2 = d.right() + 1, d.bottom() + 1

            im_face = im[y1:y2, x1:x2]

            face_list.append(im_face)
            coordinate_list.append([x1, y1, x2, y2])

        return rect, face_list, coordinate_list

    @staticmethod
    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def detect_age_gender_single_face(self, im):

        faces = np.empty((1, self.age_gender_img_size, self.age_gender_img_size, 3))

        faces[0, :, :, :] = cv2.resize(im, (self.age_gender_img_size, self.age_gender_img_size))

        results = self.age_gender_model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        ret_age_gender = [int(predicted_ages[0]), "Female" if predicted_genders[0][0] > 0.5 else "Male"]

        return ret_age_gender

    def predict_age_and_gender(self, face_list):
        result = []
        for i in range(len(face_list)):
            result.append(self.detect_age_gender_single_face(face_list[i]))

        return result
