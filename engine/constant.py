# Google API constants
CLOUD_VIDEO_UPLOAD_FOLDER = 'upload_videos'
CLOUD_ANAL_JSON_FOLDER = 'analysis_json'

GOOGLE_CREDENTIAL_FILE = "config/graceful-karma-366110-848d84d09f69.json"
GOOGLE_PROJECT_NAME = 'My First Project'
GOOGLE_STORAGE_BUCKET_NAME = 'poc-bucket-sett-video'

# Color detection constants
COLOR_DET_NUM_CLUSTERS = 16
COLOR_DET_DISPLAY_COUNTS = 5
COLOR_DET_CHECK_DURATION = 3

# Video property constants
VIDEO_ASPECT_RATIO_HORIZONTAL = 'horizontal'
VIDEO_ASPECT_RATIO_VERTICAL = 'vertical'
VIDEO_ASPECT_RATIO_SQUARE = 'square'

# Age Gender detection constants
AGE_BABY = 'Baby'
AGE_TEEN = 'Teen'
AGE_ADULT = 'Adult'
AGE_OLD = 'Old'
GENDER_MALE = 'M'
GENDER_FEMALE = 'F'
GENDER_NA = 'NA'

# Output file path
OUTPUT_JSON_COLOR = 'output_color.json'
OUTPUT_JSON_FACE = 'output_face.json'
OUTPUT_JSON_GOOGLE = 'output_google.json'
OUTPUT_JSON_ALL = 'output.json'

# Field names of output feature file
FIELD_PERF_PLATFORM_ID = 'Perf_platformId'
FIELD_PERF_DIM_RATIO = 'Perf_dimRatio'
FIELD_PERF_VIDEO_DURATION = 'Perf_videoDuration'
FIELD_COLOR_BRIGHTNESS = 'Ext_colorBrightness'
FIELD_COLOR_WORMTH = 'Ext_colorWormth'
FIELD_IS_DOMINANT_COLOR = 'Ext_isDominantColor'
FIELD_DOMINANT_COLOR = 'Ext_dominantColor'
FIELD_IS_3S_FACE = 'Ext_is3SecondDominantFace'
FIELD_3S_GENDER = 'Ext_3SecondDominantFaceGender'
FIELD_3S_AGE = 'Ext_3SecondDominantFaceAgeGroup'
FIELD_3S_FACES = 'Ext_3SecondFaces'
FIELD_IS_10S_FACE = 'Ext_is3to10SecondDominantFace'
FIELD_10S_GENDER = 'Ext_3to10SecondDominantFaceGender'
FIELD_10S_AGE = 'Ext_3to10SecondDominantFaceAgeGroup'
FIELD_10S_FACES = 'Ext_3to10SecondFaces'
FIELD_3S_VOICE_WORDS = 'Ext_numTranscriptionWords3Sec'
FIELD_10S_VOICE_WORDS = 'Ext_numTranscriptionWords3to10Sec'
FIELD_3S_VOICE_WORDS_MOOD = 'Ext_TranscriptionWords3SecMood'
FIELD_10S_VOICE_WORDS_MOOD = 'Ext_numTranscriptionWords3to10SecMood'
FIELD_3S_TEXT_WORDS = 'Ext_numOCRWords3Sec'
FIELD_10S_TEXT_WORDS = 'Ext_numOCRWords3to10Sec'
FIELD_3S_TEXT_WORDS_MOOD = 'Ext_OCRWords3SecMood'
FIELD_10S_TEXT_WORDS_MOOD = 'Ext_numOCRWords3to10SecMood'
FIELD_AUDIO_MOOD = "Ext_audioMood"
FIELD_SCORE = 'score'

# Audio Mood constants
AUDIO_MOOD_HAPPY = 'Happy'
AUDIO_MOOD_EXUBERANT = 'Exuberant'
AUDIO_MOOD_ENERGETIC = 'Energetic'
AUDIO_MOOD_FRANTIC = 'Frantic'
AUDIO_MOOD_ANXIOUS = 'Anxious'
AUDIO_MOOD_DEPRESSION = 'Depression'
AUDIO_MOOD_CALM = 'Calm'
AUDIO_MOOD_CONTENTMENT = 'Contentment'
