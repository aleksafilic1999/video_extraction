import cv2
import numpy as np


MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
GENDER_LIST = ['Male', 'Female']
AGE_INTERVALS = ['(0, 2)', '(4, 6)', '(8, 12)', '(15, 20)', '(25, 32)', '(38, 43)', '(48, 53)', '(60, 100)']


class FaceEngineDNN:

    def __init__(self):
        self.face_net = cv2.dnn.readNetFromCaffe("models_dnn/deploy.prototxt.txt",
                                                 "models_dnn/res10_300x300_ssd_iter_140000_fp16.caffemodel")
        self.age_net = cv2.dnn.readNetFromCaffe('models_dnn/deploy_age.prototxt',
                                                'models_dnn/age_net.caffemodel')
        self.gender_net = cv2.dnn.readNetFromCaffe('models_dnn/deploy_gender.prototxt',
                                                   'models_dnn/gender_net.caffemodel')
        self.max_frame_width = 1280

    def get_faces(self, img, confidence_threshold=0.5):
        blob = cv2.dnn.blobFromImage(img, 1.0, (300, 300), (104, 177.0, 123.0))
        self.face_net.setInput(blob)
        output = np.squeeze(self.face_net.forward())

        face_rects = []
        face_coordinates = []
        face_images = []
        for ind in range(output.shape[0]):
            confidence = output[ind, 2]
            if confidence > confidence_threshold:
                box = output[ind, 3:7] * \
                    np.array([img.shape[1], img.shape[0],
                             img.shape[1], img.shape[0]])
                # convert to integers
                start_x, start_y, end_x, end_y = box.astype(np.int)
                # widen the box a little
                start_x, start_y, end_x, end_y = start_x - \
                    10, start_y - 10, end_x + 10, end_y + 10
                start_x = 0 if start_x < 0 else start_x
                start_y = 0 if start_y < 0 else start_y
                end_x = 0 if end_x < 0 else end_x
                end_y = 0 if end_y < 0 else end_y
                # append to our list
                face_coordinates.append((start_x, start_y, end_x, end_y))
                face_images.append(img[start_y: end_y, start_x: end_x])

        return face_rects, face_images, face_coordinates

    @staticmethod
    def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
        (h, w) = image.shape[:2]
        if width is None and height is None:
            return image

        if width is None:
            dim = (int(w * height / float(h)), height)
        else:
            dim = (width, int(h * width / float(w)))

        return cv2.resize(image, dim, interpolation=inter)

    def get_gender_predictions(self, face_img):
        blob = cv2.dnn.blobFromImage(
            image=face_img, scalefactor=1.0, size=(227, 227),
            mean=MODEL_MEAN_VALUES, swapRB=False, crop=False
        )
        self.gender_net.setInput(blob)
        return self.gender_net.forward()

    def get_age_predictions(self, face_img):
        blob = cv2.dnn.blobFromImage(
            image=face_img, scalefactor=1.0, size=(227, 227),
            mean=MODEL_MEAN_VALUES, swapRB=False
        )
        self.age_net.setInput(blob)
        return self.age_net.forward()

    def predict_age_and_gender(self, face_images):
        result = []
        for face_img in face_images:
            age_preds = self.get_age_predictions(face_img)
            gender_preds = self.get_gender_predictions(face_img)
            opt_ind = gender_preds[0].argmax()
            gender = GENDER_LIST[opt_ind]
            gender_confidence_score = gender_preds[0][opt_ind]
            opt_ind = age_preds[0].argmax()
            age = AGE_INTERVALS[opt_ind]
            age_confidence_score = age_preds[0][opt_ind]
            result.append([age, gender])

        return result


if __name__ == "__main__":
    class_face = FaceEngineDNN()
    cap = cv2.VideoCapture('../video/3.mp4')
    while True:
        r, frame = cap.read()
        if not r:
            break

        if frame.shape[1] > class_face.max_frame_width:
            frame = class_face.image_resize(frame, width=class_face.max_frame_width)

        _, face_img_list, face_pos_list = class_face.get_faces(frame)
        age_gender_list = class_face.predict_age_and_gender(face_img_list)
        print(age_gender_list)
