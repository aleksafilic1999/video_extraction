## Packages

        pip install opencv-python==4.2.*
        pip install keras
        pip install dlib
        pip install deepface
        pip install google-cloud-videointelligence
        pip install google-cloud-speech
        pip install google-cloud-storage
        pip install scipy
        pip install pygad
        pip install moviepy
        pip install librosa
        pip install PyTimbre


## Installing FFmpeg
- Windows:

        Download the build from here. https://www.gyan.dev/ffmpeg/builds/
        Unzip the build in any folder.
        Open the CMD with adminstrative rights.
        Run the below command for setting the environment variable.
        setx /M PATH "path\to\ffmpeg\bin;%PATH%"

- Linux:
    Write the below commands in the terminal.

        sudo add-apt-repository ppa:mc3man/trusty-media  
        sudo apt-get update  
        sudo apt-get install ffmpeg  
        sudo apt-get install frei0r-plugins

        
## Extract video feature and store as feature csv file

        python  video_feature.py  video_path  [reference_csv_file]

`video_path` can be video file name or video folder path

For example:

        python video_feature.py  1.mp4
        python video_feature.py  2_wy3l3n08.mp4  ../document/creative-analysis-1481009631.csv
        python video_feature.py  ../video_folder  ../document/creative-analysis-1481009631.csv
        
If user ignore reference_csv_file, the engine will not use csv file and empty those fields.
After running script, the feature csv file "output_feature.csv" and upgrade feature csv file for training "output_upgraded_feature.csv" will be generated. 

## Convert feature csv file to upgrade csv file for training

        python  upgrade_feature.py  feature_csv_filename  [upgrade_csv_filename]
        
Default value:
            
        upgrade_csv_filename: 'output_upgraded_feature.csv' 
        
For example:

        python upgrade_feature.py  output_feature.csv
        python upgrade_feature.py  output_feature.csv  output_upgraded_feature.csv
