from engine.constant import *
from engine.func import *
from engine import ga
import sys


def upgrade_feature(key_list, feature):
    new_feature = []
    for key_ind in range(len(key_list)):
        val = feature[key_ind]
        if key_list[key_ind] == FIELD_COLOR_BRIGHTNESS:
            new_val = upgrade_float_list(val, range_list=[83, 148, 200], value_list=[2, 3, 4, 1])
        elif key_list[key_ind] == FIELD_COLOR_WORMTH:
            new_val = upgrade_float_list(val, range_list=[30.51, 64.11, 86.51], value_list=[1, 2, 3, 4])

        elif key_list[key_ind] == FIELD_IS_DOMINANT_COLOR:
            new_val = upgrade_string_list(val, string_list=['False', 'True'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_IS_3S_FACE:
            new_val = upgrade_string_list(val, string_list=['False', 'True'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_3S_FACES:
            new_val = upgrade_float_list(val, range_list=[0, 1], value_list=[0, 1, 2])
        elif key_list[key_ind] == FIELD_IS_10S_FACE:
            new_val = upgrade_string_list(val, string_list=['False', 'True'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_10S_FACES:
            new_val = upgrade_float_list(val, range_list=[0, 1], value_list=[0, 1, 2])
        elif key_list[key_ind] == FIELD_3S_AGE:
            new_val = upgrade_string_list(val,
                                          string_list=[AGE_BABY, AGE_TEEN, AGE_ADULT, AGE_OLD],
                                          value_list=[1, 2, 3, 4, 0])
        elif key_list[key_ind] == FIELD_3S_GENDER:
            new_val = upgrade_string_list(val, string_list=[GENDER_MALE, GENDER_FEMALE], value_list=[1, 2, 0])
        elif key_list[key_ind] == FIELD_10S_AGE:
            new_val = upgrade_string_list(val,
                                          string_list=[AGE_BABY, AGE_TEEN, AGE_ADULT, AGE_OLD],
                                          value_list=[1, 2, 3, 4, 0])
        elif key_list[key_ind] == FIELD_10S_GENDER:
            new_val = upgrade_string_list(val,
                                          string_list=[GENDER_MALE, GENDER_FEMALE],
                                          value_list=[1, 2, 0])
        elif key_list[key_ind] == FIELD_3S_TEXT_WORDS:
            new_val = upgrade_float_list(val, range_list=[0, 10], value_list=[0, 1, 2])
        elif key_list[key_ind] == FIELD_10S_TEXT_WORDS:
            new_val = upgrade_float_list(val, range_list=[0, 10], value_list=[0, 1, 2])
        elif key_list[key_ind] == FIELD_3S_TEXT_WORDS_MOOD:
            new_val = upgrade_string_list(val, string_list=['No', 'Yes'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_10S_TEXT_WORDS_MOOD:
            new_val = upgrade_string_list(val, string_list=['No', 'Yes'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_3S_VOICE_WORDS:
            new_val = upgrade_float_list(val, range_list=[0], value_list=[0, 1])
        elif key_list[key_ind] == FIELD_10S_VOICE_WORDS:
            new_val = upgrade_float_list(val, range_list=[0], value_list=[0, 1])
        elif key_list[key_ind] == FIELD_3S_VOICE_WORDS_MOOD:
            new_val = upgrade_string_list(val, string_list=['No', 'Yes'], value_list=[0, 1, 0])
        elif key_list[key_ind] == FIELD_10S_VOICE_WORDS_MOOD:
            new_val = upgrade_string_list(val, string_list=['No', 'Yes'], value_list=[0, 1, 0])
        elif key_list[key_ind] == 'firstSeen':
            new_val = upgrade_float_list(val, range_list=[0, 57], value_list=[0, 1, 2])
        elif key_list[key_ind] == 'lastSeen':
            new_val = upgrade_float_list(val, range_list=[10, 42], value_list=[0, 1, 2])
        elif key_list[key_ind] == 'duration':
            new_val = upgrade_float_list(val, range_list=[10, 30], value_list=[1, 2, 3])
        elif key_list[key_ind] == FIELD_PERF_DIM_RATIO:
            new_val = upgrade_string_list(val,
                                          string_list=[VIDEO_ASPECT_RATIO_HORIZONTAL, VIDEO_ASPECT_RATIO_VERTICAL],
                                          value_list=[1, 2, 3])
        elif key_list[key_ind] == 'impressionShare':
            new_val = upgrade_float_list(val, range_list=[0.15, 0.35, 0.65, 1], value_list=[1, 2, 3, 4, 5])
        elif key_list[key_ind] == 'category':
            new_val = upgrade_string_list(val,
                                          string_list=['games', 'word', 'casual', 'adventure', 'puzzle'],
                                          value_list=[1, 2, 3, 4, 5, 0])
        elif key_list[key_ind] == FIELD_AUDIO_MOOD:
            string_list = [AUDIO_MOOD_HAPPY, AUDIO_MOOD_EXUBERANT, AUDIO_MOOD_ENERGETIC, AUDIO_MOOD_FRANTIC,
                           AUDIO_MOOD_ANXIOUS, AUDIO_MOOD_DEPRESSION, AUDIO_MOOD_CALM, AUDIO_MOOD_CONTENTMENT]
            new_val = upgrade_string_list(val, string_list=string_list, value_list=[1, 2, 3, 4, 5, 6, 7, 8, 0])
        else:
            new_val = val

        new_feature.append(new_val)

    return new_feature


def calculate_score(key_list, feature):
    def find_value(k):
        if k in key_list:
            return feature[key_list.index(k)]
        else:
            return 0

    score_input = [
        find_value(FIELD_COLOR_BRIGHTNESS),
        find_value(FIELD_COLOR_WORMTH),
        find_value(FIELD_3S_FACES),
        find_value(FIELD_10S_FACES),
        find_value(FIELD_3S_AGE),
        find_value(FIELD_3S_GENDER),
        find_value(FIELD_10S_AGE),
        find_value(FIELD_10S_GENDER),
        find_value(FIELD_3S_TEXT_WORDS),
        find_value(FIELD_10S_TEXT_WORDS),
        find_value(FIELD_3S_TEXT_WORDS_MOOD),
        find_value(FIELD_10S_TEXT_WORDS_MOOD),
        find_value(FIELD_3S_VOICE_WORDS),
        find_value(FIELD_10S_VOICE_WORDS),
        find_value(FIELD_3S_VOICE_WORDS_MOOD),
        find_value(FIELD_10S_VOICE_WORDS_MOOD),
        find_value('category'),
        1 if find_value(FIELD_PERF_DIM_RATIO) == 1 else 0,
        1 if find_value(FIELD_PERF_DIM_RATIO) == 2 else 0,
        1 if find_value(FIELD_PERF_DIM_RATIO) == 3 else 0
    ]

    score = ga.expression(score_input)

    return score


def convert_advanced_csv(in_csv_filename, out_csv_filename):
    feature_list = read_csv(in_csv_filename)
    if len(feature_list) == 0:
        return

    key_list = feature_list[0]
    new_feature_list = [key_list]
    for row_ind in range(1, len(feature_list)):
        # convert to advanced feature list
        new_feature = upgrade_feature(key_list, feature_list[row_ind])

        # calculate score and update into advanced feature list
        score = calculate_score(key_list, new_feature)
        new_feature[key_list.index('score')] = score

        new_feature_list.append(new_feature)

    # save to csv
    save_csv(out_csv_filename, new_feature_list)


if __name__ == '__main__':
    in_arg = ['output_feature.csv',
              'output_upgraded_feature.csv']

    for arg_ind in range(1, len(sys.argv)):
        in_arg[arg_ind - 1] = sys.argv[arg_ind]

    convert_advanced_csv(in_csv_filename=in_arg[0], out_csv_filename=in_arg[1])
